package net.zoranpavlovic.data

import android.content.Context
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.os.Handler
import android.os.HandlerThread
import android.provider.MediaStore.Images.ImageColumns
import android.provider.MediaStore.Images.Media
import android.provider.MediaStore.MediaColumns
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import net.zoranpavlovic.domain.photos.Photo
import net.zoranpavlovic.domain.photos.PhotosRepository
import java.util.Collections

class PhotosLocalStorageRepository(private val appContext: Context) : PhotosRepository {
  private val handlerThread = HandlerThread("ContentObserver")
  private val handler: Handler

  private val contentResolver = appContext.contentResolver
  private val uri: Uri = Media.EXTERNAL_CONTENT_URI

  init {
    handlerThread.start()
    handler = Handler(handlerThread.looper)
  }

  override fun getAll(): Flowable<List<Photo>> {
    return Flowable.create<List<Photo>>({ emitter ->
        emitter.onNext(getAllPhotos())

        val contentObserver = object: ContentObserver(handler) {
          override fun onChange(selfChange: Boolean) {
            if(!emitter.isCancelled) {
              emitter.onNext(getAllPhotos())
            }
          }
        }

        emitter.setDisposable(Disposables.fromAction {
          contentResolver.unregisterContentObserver(contentObserver)
        })

        contentResolver.registerContentObserver(uri, true, contentObserver)
      }, BackpressureStrategy.LATEST
    ).subscribeOn(AndroidSchedulers.from(handlerThread.looper))
  }

  private fun getAllPhotos(): List<Photo> {
    val cursor: Cursor
    val columnIndex: Int
    val photos = ArrayList<Photo>()
    var photoPath: String?

    val projection = arrayOf(MediaColumns.DATA)
    val orderBy = ImageColumns.DATE_TAKEN + " DESC"

    cursor = contentResolver.query(uri, projection, null, null, orderBy) ?: return emptyList()
    columnIndex = cursor.getColumnIndexOrThrow(MediaColumns.DATA)

    while (cursor.moveToNext()) {
      photoPath = cursor.getString(columnIndex)
      photos.add(Photo(photoPath)
      )
    }

    cursor.close()

    return Collections.unmodifiableList(photos)
  }
}
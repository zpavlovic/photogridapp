package net.zoranpavlovic.domain.photos

import io.reactivex.Flowable
import net.zoranpavlovic.domain.core.NoParam
import net.zoranpavlovic.domain.core.UseCase

open class GetAllPhotosUseCase(private val repository: PhotosRepository) : UseCase<List<Photo>, NoParam> {

  override fun execute(p: NoParam?): Flowable<List<Photo>> {
    return repository.getAll()
  }

}
package net.zoranpavlovic.domain.photos

data class Photo(val path: String)
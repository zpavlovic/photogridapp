package net.zoranpavlovic.domain.photos

import io.reactivex.Flowable

interface PhotosRepository {
  fun getAll(): Flowable<List<Photo>>
}
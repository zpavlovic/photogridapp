package net.zoranpavlovic.domain.core

import io.reactivex.Flowable

interface UseCase<ReturnType, Param> {
  fun execute(p: NoParam?): Flowable<ReturnType>
}
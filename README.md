# PhotoGrid app

The architecture of the app is inspired by Clean architecture with MVP pattern. 

It has three modules:

- app as presentation layer,

- domain module as bussiness layer,

- data module as data layer.


I have used only RxJava 2 library to observe for taking a new photos and to load them in async way. The main reason, is that I did not want to use outdated Loaders, as they are deprecated: https://developer.android.com/guide/components/loaders

Usually, I would also use Dagger as Depedncy Injection library.

There are also unit tests for Presenter and Use Case, just to demonstrate it.

# Images

![Main screen](/main.png)
![Details screen](/details.png)

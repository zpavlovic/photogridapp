package net.zoranpavlovic.photogridapp.main

import android.Manifest.permission
import android.app.AlertDialog.Builder
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.zoranpavlovic.data.PhotosLocalStorageRepository
import net.zoranpavlovic.domain.photos.GetAllPhotosUseCase
import net.zoranpavlovic.domain.photos.Photo
import net.zoranpavlovic.photogridapp.R
import net.zoranpavlovic.photogridapp.R.id
import net.zoranpavlovic.photogridapp.R.layout
import net.zoranpavlovic.photogridapp.R.string
import net.zoranpavlovic.photogridapp.details.PhotoDetailsActivity
import net.zoranpavlovic.photogridapp.main.MainAdapter.PhotosListener
import net.zoranpavlovic.photogridapp.main.MainPresenter.View

class MainActivity : AppCompatActivity(), PhotosListener, View {

  companion object {
    private const val REQUEST_CODE = 1
    private const val SETTINGS_REQUEST_CODE = 2;
  }

  private lateinit var toolbar: Toolbar
  private lateinit var rvPhotos: RecyclerView
  private lateinit var presenter: MainPresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(layout.activity_main)

    initPresenter()

    setUpUi()

    loadPhotos()
  }

  private fun setUpUi() {
    setUpToolbar()

    setUpRecyclerView()
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter.dispose()
  }

  private fun loadPhotos() {
    if (isReadExternalStoragePermissionGranted()) {
      presenter.getAllPhotos()
    } else {
      requestPermission()
    }
  }

  private fun initPresenter() {
    val repository = PhotosLocalStorageRepository(applicationContext)
    val getPhotosUseCase =
      GetAllPhotosUseCase(repository)
    presenter =
      MainPresenter(getPhotosUseCase)
    presenter.view = this
  }

  private fun setUpToolbar() {
    toolbar = findViewById(id.activityMainToolbar)
    setSupportActionBar(toolbar)
    toolbar.title = getString(R.string.app_name)
  }

  private fun setUpRecyclerView() {
    rvPhotos = findViewById(id.activityMainRvPhotos)
    rvPhotos.layoutManager = GridLayoutManager(this, 3)
  }

  private fun requestPermission() {
    ActivityCompat.requestPermissions(this, arrayOf(permission.READ_EXTERNAL_STORAGE),
                                      REQUEST_CODE
    )
  }

  private fun isReadExternalStoragePermissionGranted(): Boolean {
    val permission = ContextCompat.checkSelfPermission(this, permission.READ_EXTERNAL_STORAGE)
    return permission == PackageManager.PERMISSION_GRANTED
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
  ) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    if (grantResults.isEmpty() || grantResults[0] == PackageManager.PERMISSION_DENIED) {
      showSettingsDialog()
    } else {
      presenter.getAllPhotos()
    }
  }

  override fun onPhotoClick(photo: Photo) {
    val intent = PhotoDetailsActivity.newIntent(this, photo)
    startActivity(intent)
  }

  override fun displayPhotos(photos: List<Photo>) {
    val adapter = MainAdapter(photos = photos, listener = this)
    rvPhotos.adapter = adapter
  }

  private fun showSettingsDialog() {
    Builder(this)
      .setTitle(getString(string.permission_title))
      .setMessage(getString(string.permission_location_message))
      .setPositiveButton(getString(string.permission_go_to_settings_button)) { dialog, which ->
        dialog.cancel()
        openSettings()
      }
      .setNegativeButton(
        getString(string.permission_cancel_button)
      ) { dialog, _ ->
        dialog.cancel()
      }
      .setCancelable(false)
      .show()
      .create()
  }

  private fun openSettings() {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
    val uri = Uri.fromParts("package", packageName, null)
    intent.data = uri
    startActivityForResult(intent, SETTINGS_REQUEST_CODE
    )
  }

  override fun onActivityResult(
    requestCode: Int, resultCode: Int, data: Intent?
  ) {
    super.onActivityResult(requestCode, resultCode, data)
    if (requestCode == SETTINGS_REQUEST_CODE) {
      if (isReadExternalStoragePermissionGranted()) {
        presenter.getAllPhotos()
      }
    }
    super.onActivityResult(requestCode, resultCode, data)
  }
}
package net.zoranpavlovic.photogridapp.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_photo.view.ivPhoto
import net.zoranpavlovic.domain.photos.Photo
import net.zoranpavlovic.photogridapp.R.layout
import net.zoranpavlovic.photogridapp.main.MainAdapter.ViewHolder
import java.io.File

class MainAdapter(private val photos: List<Photo>, private val listener: PhotosListener)
  : RecyclerView.Adapter<ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val itemView: View = LayoutInflater.from(parent.context).inflate(layout.list_item_photo, parent,
                                                                     false)
    return ViewHolder(itemView)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bindData(photos[position])
  }

  override fun getItemCount(): Int = photos.size

  inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindData(photo: Photo) {
      val f = File(photo.path)

      Picasso.get()
        .load(f)
        .centerCrop()
        .fit()
        .into(itemView.ivPhoto)

      itemView.ivPhoto.setOnClickListener {
        listener.onPhotoClick(photo)
      }
    }
  }

  interface PhotosListener {
    fun onPhotoClick(photo: Photo)
  }

}
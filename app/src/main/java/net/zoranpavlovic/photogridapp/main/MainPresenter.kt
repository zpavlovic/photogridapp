package net.zoranpavlovic.photogridapp.main

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import net.zoranpavlovic.domain.core.NoParam
import net.zoranpavlovic.domain.photos.GetAllPhotosUseCase
import net.zoranpavlovic.domain.photos.Photo
import net.zoranpavlovic.photogridapp.core.Presenter

open class MainPresenter(private var getAllPhotos: GetAllPhotosUseCase) : Presenter {

  private val compositeDisposable = CompositeDisposable()
  internal lateinit var view: View

  fun getAllPhotos() {
    val disposable = getAllPhotos.execute(NoParam())
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe { photos ->
        view.displayPhotos(photos = photos)
      }

    compositeDisposable.add(disposable)
  }

  override fun dispose() {
    compositeDisposable.clear()
  }

  interface View {
    fun displayPhotos(photos: List<Photo>)
  }
}
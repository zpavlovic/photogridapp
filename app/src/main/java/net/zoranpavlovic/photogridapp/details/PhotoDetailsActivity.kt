package net.zoranpavlovic.photogridapp.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_photo_details.ivPhotoDetails
import net.zoranpavlovic.domain.photos.Photo
import net.zoranpavlovic.photogridapp.R.id
import net.zoranpavlovic.photogridapp.R.layout
import net.zoranpavlovic.photogridapp.R.string
import java.io.File

class PhotoDetailsActivity : AppCompatActivity() {
  companion object {
    fun newIntent(context: Context, photo: Photo) =
      Intent(context, PhotoDetailsActivity::class.java).apply {
        putExtra("path", photo.path)
      }
  }

  private lateinit var toolbar: Toolbar

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(layout.activity_photo_details)

    setUpToolbar()

    val path = intent.getStringExtra("path")

    Picasso.get().load(File(path)).into(ivPhotoDetails)
  }

  private fun setUpToolbar() {
    toolbar = findViewById(id.activityPhotoDetailsToolbar)
    setSupportActionBar(toolbar)
    toolbar.title = getString(string.app_name)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    if (item.itemId == android.R.id.home) {
      onBackPressed()
      return true
    }
    return super.onOptionsItemSelected(item)
  }
}
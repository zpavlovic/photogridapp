package net.zoranpavlovic.photogridapp.core

interface Presenter {

  fun dispose()
}
package net.zoranpavlovic.photogridapp.main

import io.reactivex.Flowable
import net.zoranpavlovic.domain.photos.GetAllPhotosUseCase
import net.zoranpavlovic.domain.photos.Photo
import net.zoranpavlovic.domain.photos.PhotosRepository
import net.zoranpavlovic.photogridapp.core.TrampolineSchedulerRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.powermock.modules.junit4.PowerMockRunner
import java.util.ArrayList

@RunWith(PowerMockRunner::class)
class MainPresenterTest {

  @Rule var trampolineSchedulerRule: TrampolineSchedulerRule = TrampolineSchedulerRule()
  @Mock lateinit var getAllPhotosUseCase: GetAllPhotosUseCase
  @Mock lateinit var repository: PhotosRepository
  @Mock lateinit var view: MainPresenter.View
  @Mock lateinit var presenter: MainPresenter

  @Before fun setUp() {
    getAllPhotosUseCase = GetAllPhotosUseCase(repository)
    presenter = MainPresenter(getAllPhotosUseCase)
    presenter.view = view
  }

  @Test
  fun getAllPhotos() {
    // Given
    val photos: List<Photo> = ArrayList<Photo>()

    // When
    Mockito.`when`<Any>(getAllPhotosUseCase.execute(null)).thenReturn(Flowable.just(photos))
    presenter.getAllPhotos()

    // Then
    Mockito.verify(view).displayPhotos(photos)
  }
}
package net.zoranpavlovic.photogridapp.core

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import java.util.concurrent.Callable

class TrampolineSchedulerRule : TestRule {
  override fun apply(
    base: Statement,
    description: Description
  ): Statement {
    return object : Statement() {
      @Throws(Throwable::class) override fun evaluate() {
        RxJavaPlugins.setIoSchedulerHandler { a: Scheduler? -> Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { a: Scheduler? -> Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { a: Scheduler? -> Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { a: Callable<Scheduler?>? -> Schedulers.trampoline() }
        try {
          base.evaluate()
        } finally {
          RxJavaPlugins.reset()
          RxAndroidPlugins.reset()
        }
      }
    }
  }
}
package net.zoranpavlovic.photogridapp.photos

import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import net.zoranpavlovic.domain.photos.GetAllPhotosUseCase
import net.zoranpavlovic.domain.photos.Photo
import net.zoranpavlovic.domain.photos.PhotosRepository
import net.zoranpavlovic.photogridapp.core.TrampolineSchedulerRule
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.powermock.modules.junit4.PowerMockRunner
import java.util.ArrayList

@RunWith(PowerMockRunner::class)
class GetAllPhotosUseCaseTest {

  @Rule var trampolineSchedulerRule: TrampolineSchedulerRule = TrampolineSchedulerRule()
  @Mock lateinit var repository: PhotosRepository
  private lateinit var getAllPhotosUseCase: GetAllPhotosUseCase

  @Before
  fun setUp() {
    getAllPhotosUseCase = GetAllPhotosUseCase(repository)
  }

  @Test fun shouldGetAllCategoriesWithNotEmptyList() {
    // Given
    val photos: MutableList<Photo> = ArrayList<Photo>()
    photos.add(Photo("path"))

    // When
    Mockito.`when`<Any>(repository.getAll()).thenReturn(Flowable.just<List<Photo>>(photos))
    val testObserver: TestSubscriber<List<Photo>>? = getAllPhotosUseCase.execute(null).test()

    // Then
    val result: List<Photo> = testObserver!!.values()[0]
    Assert.assertEquals(photos.size.toLong(), result.size.toLong())
    Assert.assertThat(result.isEmpty(), CoreMatchers.`is`(false))
  }
}
